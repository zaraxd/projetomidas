'use strict'

class Utils {

    convertUnit(value, code) {
        if (code === 1 || code === 4) {
            return value / 1000
        } else {
            return value
        }
    }

    markup(value, margin) {
        return value + (value * (margin / 100))
    }

    format(value) {
        return Math.trunc(value * 100) / 100
    }

}

module.exports = Utils