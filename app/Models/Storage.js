'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Storage extends Model {
    products () {
        return this.belongsTo('App/Models/Product')
    }

    batches () {
        return this.hasMany('App/Models/Batch')
    }
}

module.exports = Storage
