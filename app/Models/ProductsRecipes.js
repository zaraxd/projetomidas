'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ProductsRecipes extends Model {
    products () {
        return this.belongsTo('App/Models/Product')
    }

    recipes () {
        return this.belongsTo('App/Models/Recipe')
    }
}

module.exports = ProductsRecipes
