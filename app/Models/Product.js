'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Product extends Model {
	units () {
		return this.hasOne('App/Models/Unit')
	}

    products_recipes () {
        return this.hasMany('App/Models/ProductsRecipes')
    }

    storages () {
        return this.hasOne('App/Models/Storage')
    }
}

module.exports = Product
