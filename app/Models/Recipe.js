'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Recipe extends Model {
    products_recipes () {
        return this.hasMany('App/Models/ProductsRecipes')
    }
}

module.exports = Recipe
