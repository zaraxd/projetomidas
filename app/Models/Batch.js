'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Batch extends Model {
    storages () {
        return this.belongsTo('App/Models/Storage')
    }
}

module.exports = Batch
