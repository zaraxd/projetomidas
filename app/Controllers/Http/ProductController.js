'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

const Product = use('App/Models/Product')
const Storage = use('App/Models/Storage')
const ProductsRecipes = use('App/Models/ProductsRecipes')

class ProductController {

  // Função onde mostra todos os produtos salvos no banco de dados

    async index() {
        const products = await Product.query().with('storages').fetch()
        return products
    }

  // Função onde salva um novo produto no banco de dados.

    async store({ request, response }) {
        const data = request.only(['name', 'quantity', 'price', 'unit_id'])
        const dbResponse = await Product.findBy('name', data.name)
        if (!dbResponse) {
            const dataObj = {
                name: data.name,
                unit_price: data.price / parseFloat(data.quantity),
                unit_id: data.unit_id
            }
            const product = await Product.create({ ...dataObj })
            const storage = await Storage.findOrCreate(
                { product_id: product.id },
                { product_id: product.id, quantity: data.quantity })
            response.status(200).send({ product: product, storage: storage })
        } else {
            response.status(406).send({ message: 'Este produto já existe.' })
        }
    }

  // Função onde mostra um produto específico e suas informações detalhadas.

    async show({ params, response }) {
        const product = await Product.query().with('storages').where('id', params.id).fetch()
        product.rows.length ? response.status(200).send(product) : response.status(404).send({ message: 'Produto não encontrado.' })
    }
 // Função para atualização de um determinado produto pelo seu numero de id.

    async update({ params, request, response }) {
        const data = request.only(['name', 'quantity', 'price', 'unit_id'])
        const storage = await Storage.findBy('product_id', params.id)
        if (storage) {
            storage.quantity = storage.quantity + parseFloat(data.quantity)
            storage.save()
            const product = await Product.findOrFail(params.id)
            product.unit_price = data.price / data.quantity
            product.unit_id = data.unit_id
            product.save()
            response.status(200).send(product)
        } else {
            response.status(404).send({ message: 'Produto não encontrado.' })
        }
    }

    // Função para excluir um determinado produto pelo seu numero de id
    async destroy({ params, response }) {
        const recipe = await ProductsRecipes.findBy('product_id', params.id)
        if (!recipe) {
            const storage = await Storage.findBy('product_id', params.id)
            if (storage) {
                await storage.delete()
                const product = await Product.findOrFail(params.id)
                await product.delete()
                response.status(200).send(product)
            } else {
                response.status(404).send({ message: 'Produto não encontrado.' })
            }
        } else {
            response.status(406).send({ message: 'Este produto está sendo usado em uma receita.' })
        }
    }

}

module.exports = ProductController
