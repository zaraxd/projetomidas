'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

const Recipe = use('App/Models/Recipe')
const Storage = use('App/Models/Storage')
const Utils = use('App/Services/Utils')
const Database = use('Database')


class ManufactureController {

// Função de produção de receitas, retira os ingredientes que
// serão usados do estoque e retorna quantas unidades  serão produzidas
// valor da receita total, valor da receita unitaria e
// sugestão de preço com o percentual de lucro escolhido

    async make({ request, response }) {
        const data = request.only(['recipe_id', 'quantity', 'markup'])
        const recipe = await Recipe.findBy('id', data.recipe_id)
        const info = {}
        const shortStorage = []
        const utils = new Utils()
        let recipeCost = 0

        const products = await Database.select(
            'products.id as product_id',
            'recipes.id as recipe_id',
            'recipes.name as recipe_name',
            'recipes.preparation',
            'recipes.productivity',
            'products_recipes.quantity',
            'products_recipes.unit_id',
            'products.name as product_name',
            'storages.quantity as storage',
            'products.unit_price',
            'units.code as unit_code'
        )
            .from('recipes')
            .innerJoin('products_recipes', 'recipes.id', 'products_recipes.recipe_id')
            .innerJoin('products', 'products_recipes.product_id', 'products.id')
            .innerJoin('storages', 'storages.product_id', 'products.id')
            .innerJoin('units', 'units.id', 'products.unit_id')
            .where('recipes.id', data.recipe_id)

        for (const product of products) {
            const quantity = utils.convertUnit(product.quantity, product.unit_id)
            if (quantity * data.quantity > product.storage) {
                shortStorage.push({ product: product.product_name, storage: product.storage })
            }
        }
        if (!shortStorage.length) {
            for (const product of products) {
                const quantity = utils.convertUnit(product.quantity, product.unit_id)
                const storage = await Storage.findBy('product_id', product.product_id)
                storage.quantity = product.storage - (quantity * data.quantity)
                storage.save()
                recipeCost += quantity * product.unit_price
            }
            const unitPrice = recipeCost / recipe.productivity
            const suggestPrice = utils.markup(unitPrice, data.markup)

            info.total_productivity = recipe.productivity * data.quantity
            info.recipe_cost = Number(recipeCost.toFixed(2))
            info.total_cost = Number((recipeCost * data.quantity).toFixed(2))
            info.unit_price = Number(unitPrice.toFixed(2))
            info.suggest_price = Number(suggestPrice.toFixed(2))

            response.status(200).send(info)
        } else {
            response.status(406).send({ message: 'Estoque insuficiente', products: shortStorage })
        }
    }

}

module.exports = ManufactureController
