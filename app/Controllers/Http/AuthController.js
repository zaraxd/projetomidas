'use strict'

const User = use('App/Models/User')

class AuthController {
  //Função para registrar um novo usuario
    async register({ request }) {
        const data = request.only(['username', 'email', 'password'])
        const user = await User.findOrCreate({username: data.username, email: data.email}, data)

        return user
    }

    //Função para login de um usuario
    async authenticate({ request, auth }) {
        const { email, password } = request.all()
        const token = await auth.attempt(email, password)

        return token
    }
}

module.exports = AuthController
