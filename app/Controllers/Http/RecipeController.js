'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

const Recipe = use('App/Models/Recipe')
const ProductsRecipes = use('App/Models/ProductsRecipes')

class RecipeController {

  // Função onde mostra todas as receitas salvas no banco de dados
    async index() {
        const recipes = await Recipe.query().with('products_recipes').fetch()
        return recipes
    }

  // Função onde salva uma nova receita no banco de dados.
    async store({ request, response }) {
        const data = request.only(['name', 'preparation', 'productivity', 'products'])
        const dbResponse = await Recipe.findBy('name', data.name)
        if (!dbResponse) {
            const recipeData = {
                name: data.name,
                preparation: data.preparation,
                productivity: data.productivity
            }
            const recipe = await Recipe.create({ ...recipeData })

            for (const product of data.products) {
                const productsRecipes = new ProductsRecipes()
                productsRecipes.product_id = product.id
                productsRecipes.recipe_id = recipe.id
                productsRecipes.unit_id = product.unit_id
                productsRecipes.quantity = product.quantity
                productsRecipes.save()
            }
            response.status(200).send(recipe)
        } else {
            response.status(406).send({ message: 'Esta receita já existe.' })
        }
    }

  // Função onde mostra apenas uma receita específico pelo seu id
    async show({ params, response }) {
        const recipe = await Recipe.findBy('id', params.id)
        const products = await ProductsRecipes.query().with('products').where('recipe_id', params.id).fetch()
        recipe.products_info = products
        recipe ? response.status(200).send(recipe) : response.status(404).send({ message: 'Receita não encontrada.' })
    }

  // Função para atualização de uma determinada receita pelo seu numero de id
    async update({ params, request, response }) {
        const data = request.only(['name', 'preparation', 'productivity', 'products'])
        const recipe = await Recipe.findBy('id', params.id)
        if (recipe) {
            recipe.name = data.name,
            recipe.preparation = data.preparation,
            recipe.productivity = data.productivity
            recipe.save()

            const products = await ProductsRecipes.query().where('recipe_id', params.id).fetch()
            for (const product of products.rows) {
                product.delete()
            }
            for (const product of data.products) {
                const productsRecipes = new ProductsRecipes()
                productsRecipes.product_id = product.id
                productsRecipes.recipe_id = recipe.id
                productsRecipes.unit_id = product.unit_id
                productsRecipes.quantity = product.quantity
                productsRecipes.save()
            }
            response.status(200).send(recipe)
        } else {
            response.status(404).send({ message: 'Produto não encontrado.' })
        }
    }

 // Função para excluir uma determinada receita pelo seu numero de id
    async destroy({ params, response }) {
        const products = await ProductsRecipes.query().where('recipe_id', params.id).fetch()
        if (products.rows.length) {
            for (const product of products.rows) {
                await product.delete()
            }
            const recipe = await Recipe.findBy('id', params.id)
            await recipe.delete()
            response.status(200).send(recipe)
        } else {
            response.status(404).send({ message: 'Receita não encontrada.' })
        }
    }
}

module.exports = RecipeController
