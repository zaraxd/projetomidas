'use strict'

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.post('/register', 'AuthController.register')

Route.post('/authenticate', 'AuthController.authenticate')

//auth test
Route.get('/login', 'AppController.index')
// .middleware(['auth'])

//Routes for product
Route.get('/product', 'ProductController.index')
// .middleware(['auth'])

Route.get('/product/:id', 'ProductController.show')
// .middleware(['auth'])

Route.post('/product', 'ProductController.store')
// .middleware(['auth'])

Route.put('/product/:id', 'ProductController.update')
// .middleware(['auth'])

Route.delete('/product/:id', 'ProductController.destroy')
// .middleware(['auth'])

//Routes for recipe
Route.get('/recipe', 'RecipeController.index')
// .middleware(['auth'])

Route.get('/recipe/:id', 'RecipeController.show')
// .middleware(['auth'])

Route.post('/recipe', 'RecipeController.store')
// .middleware(['auth'])

Route.put('/recipe/:id', 'RecipeController.update')
// .middleware(['auth'])

Route.delete('/recipe/:id', 'RecipeController.destroy')
// .middleware(['auth'])

//Routes for manufacture
Route.post('/manufacture', 'ManufactureController.make')
// .middleware(['auth'])