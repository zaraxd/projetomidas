'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class BatchSchema extends Schema {
  up () {
    this.create('batches', (table) => {
      table.increments()
      table.integer('storage_id').notNullable().unsigned().references('id').inTable('storages')
      table.date('shelf_life')
      table.timestamps()
    })
  }

  down () {
    this.drop('batches')
  }
}

module.exports = BatchSchema
