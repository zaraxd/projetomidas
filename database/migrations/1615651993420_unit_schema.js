'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UnitSchema extends Schema {
  up () {
    this.create('units', (table) => {
      table.increments()
      table.integer('code').notNullable().unsigned().unique()
      table.string('unit', 20).unique()
    })
  }

  down () {
    this.drop('units')
  }
}

module.exports = UnitSchema
