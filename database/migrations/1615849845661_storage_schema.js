'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StorageSchema extends Schema {
  up () {
    this.create('storages', (table) => {
      table.increments()
      table.integer('product_id').notNullable().unsigned().references('id').inTable('products')
      table.decimal('quantity').notNullable().unsigned()
      table.timestamps()
    })
  }

  down () {
    this.drop('storages')
  }
}

module.exports = StorageSchema
