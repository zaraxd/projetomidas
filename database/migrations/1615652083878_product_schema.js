'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProductSchema extends Schema {
  up () {
    this.create('products', (table) => {
      table.increments()
      table.integer('unit_id').notNullable().unsigned().references('id').inTable('units').onDelete('cascade')
      table.string('name', 50).notNullable().unique()
      table.decimal('unit_price').notNullable().unsigned()
      table.timestamps()
    })
  }

  down () {
    this.drop('products')
  }
}

module.exports = ProductSchema
