'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RoleSchema extends Schema {
  up () {
    this.create('roles', (table) => {
      table.increments()
      table.integer('code').notNullable().unsigned().unique()
      table.string('role', 50).notNullable().unique()
      table.string('description', 80)
    })
  }

  down () {
    this.drop('roles')
  }
}

module.exports = RoleSchema
