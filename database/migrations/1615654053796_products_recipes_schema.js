'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProductsRecipesSchema extends Schema {
  up () {
    this.create('products_recipes', (table) => {
      table.increments()
      table.integer('product_id').notNullable().unsigned().references('id').inTable('products').onDelete('cascade')
      table.integer('recipe_id').notNullable().unsigned().references('id').inTable('recipes').onDelete('cascade')
      table.integer('unit_id').notNullable().unsigned().references('id').inTable('units').onDelete('cascade')
      table.decimal('quantity').unsigned()
      table.timestamps()
    })
  }

  down () {
    this.drop('products_recipes')
  }
}

module.exports = ProductsRecipesSchema
