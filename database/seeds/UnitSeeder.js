'use strict'

const Database = use('Database')

class UnitSeeder {
  async run () {
    await Database.table('units').insert(
      {
        code: 1,
        unit: 'Miligrama (g)',
      }
    ),
    await Database.table('units').insert(
      {
        code: 2,
        unit: 'Quilograma (kg)',
      }
    ),
    await Database.table('units').insert(
      {
        code: 3,
        unit: 'Litro (L)',
      }
    ),
    await Database.table('units').insert(
      {
        code: 4,
        unit: 'Mililitro (mL)',
      }
    ),
    await Database.table('units').insert(
      {
        code: 5,
        unit: 'Unidade (un)',
      }
    )
  }
}

module.exports = UnitSeeder
