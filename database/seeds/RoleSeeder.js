'use strict'

const Database = use('Database')

class RoleSeeder {
  async run () {
    await Database.table('roles').insert(
      {
        code: 1,
        role: 'Administrador',
        description: 'Acesso completo a todas as funcionalidades.'
      }
    ),
    await Database.table('roles').insert(
      {
        code: 2,
        role: 'Estoque',
        description: 'Acesso somente ao estoque.'
      }
    ),
    await Database.table('roles').insert(
      {
        code: 3,
        role: 'Estoque e Receitas',
        description: 'Acesso ao estoque e receitas.'
      }
    ),
    await Database.table('roles').insert(
      {
        code: 4,
        role: 'Estoque e produção',
        description: 'Acesso ao estoque e produção.'
      }
    ),
    await Database.table('roles').insert(
      {
        code: 5,
        role: 'Receitas',
        description: 'Acesso somente a receitas.'
      }
    ),
    await Database.table('roles').insert(
      {
        code: 6,
        role: 'Receitas e produção',
        description: 'Acesso as receitas e produção.'
      }
    ),
    await Database.table('roles').insert(
      {
        code: 7,
        role: 'Produção',
        description: 'Acesso somente a produção.'
      }
    ),
    await Database.table('roles').insert(
      {
        code: 8,
        role: 'Estoque, receitas e produção',
        description: 'Acesso ao estoque, receitas e produção.'
      }
    )
  }
}

module.exports = RoleSeeder
